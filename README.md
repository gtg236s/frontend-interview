# PureCars Frontend Evaluation #

Welcome! Thank you for taking time out of your busy schedules to complete our frontend evaluation project. We've all been through the interview process and know sometimes it's hard to find time for these things, but we also feel like it's the best way to get to know you as a developer. This project contains some small tasks and bug fixes so PureCars can better evaluate your skills as a frontend candidate. You have unlimited time and resources to complete the project, but as a courtesy
we think the project shouldn't take you more than two hours. To make things simple all the tooling and inital project code was created with [React Boilerplate](https://github.com/react-boilerplate/react-boilerplate/tree/master/docs). See getting started section on directions to get this project up and running.

Once you are confident in your solution please see the contribution guidelines on how to submit you project. Once you made you submission, PureCars frontend team will look over you PR. If we like what we see you'll be invited to the final in person interview.

### Evaluation ###
When the frontend team evaluates the project we will be looking over the follow criteria:

* Does it compile and work?
* Code cleanliness
* Do the unit tests actually test the code?
* Is the pull request detailed and easy to understand
* Efficiency of the code

### Requirements ###
* [Node.js >X.x.x](https://nodejs.org/en/)
* [Free bitbucket account](https://bitbucket.org/account/signup/) to submit your PR.

### Getting Started ###

#### Running locally ####

//TODO - list out steps to start local server

#### Running Unit Tests ####

//TODO - list out setup to run unit test

### What do I do? ###

* Lookover the `// TODO` comments to complete the assignment
* Code!
* Make sure to follow contribution guidelines
* [Create and submit a pull request](https://www.atlassian.com/git/tutorials/making-a-pull-request)?

### Contribution guidelines ###

* Write unit tests to any new functionality
* Make sure you follow ESLint style guides
* Submit your PR with a brief description and let your recruiter know it's ready for review

### Who do I talk to? ###

* If you have a question about the codebase or are experiencing an issue with the project feel free to [submit an issue](https://bitbucket.org/PureCarsDev/frontend-interview/issues?status=new&status=open) and we will clear things up.